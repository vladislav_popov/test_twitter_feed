import json
import requests
from apscheduler.schedulers.background import BackgroundScheduler

from flaskr.db import Database
from flaskr.follow_list import follow_list


def gett_posts(owner_id: int):
    db = Database().get_db()
    accepted_content = ['photo']  # types of attachments which post must have

    method = 'https://api.vk.com/method/wall.get'
    params = {'owner_id': owner_id,
              'access_token': '6c27d7c16c27d7c16c27d7c1776c4efcec66c276c27d7c130a4728f9dcdb33bdb9a113e',
              'v': '5.95',
              'filter': 'owner',
              'count': 100,
              'extended': 1}

    resp = requests.get(method, params=params)
    if resp.ok:
        resp_content = json.loads(resp.content)['response']
        for post in resp_content['items']:

            post_uniq_id = f'{post["owner_id"]}_{post["id"]}'  # creating unique id to check if post already cached

            if db.posts.find_one({'post_uniq_id': post_uniq_id}) is None:

                if 'attachments' in post:  # check if post has media attachments

                    # getting group name if it is group post
                    if resp_content['groups']:
                        for group in resp_content['groups']:
                            if int(group['id']) == -owner_id:
                                name = group['name']
                                ava = group['photo_50']

                    # getting profile name if it is user post
                    if resp_content['profiles']:
                        for profile in resp_content['profiles']:
                            if int(profile['id']) == owner_id:
                                name = f'{profile["first_name"]} {profile["last_name"]}'
                                ava = profile['photo_50']

                    item_to_store = {
                        'post_uniq_id': post_uniq_id,
                        'text': post['text'],
                        'owner_id': post['owner_id'],
                        'date': post['date'],
                        'attachments': [],
                        'name': name,
                        'ava': ava
                    }

                    for attachment in post['attachments']:  # looking for attachments in post
                        if attachment['type'] in accepted_content:
                            if attachment['type'] == 'photo':
                                for size in attachment['photo']['sizes']:
                                    if size['type'] == 'r':
                                        photo = {'url': size['url'], 'type': 'photo'}
                                        item_to_store['attachments'].append(photo)
                                        break
                    if item_to_store['attachments']:
                        db.posts.insert_one(item_to_store)


scheduler = BackgroundScheduler()
for id in follow_list:
    job = scheduler.add_job(gett_posts, 'interval', [id], seconds=60)
scheduler.start()


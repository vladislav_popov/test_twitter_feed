import pymongo



class Database:
    def __init__(self):
        db_uri = 'mongodb://localhost:27017/AIFactoryDB'
        client = pymongo.MongoClient(db_uri)
        self.db = client.AIFactoryDB

    def get_db(self):
        return self.db
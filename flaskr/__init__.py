import os

from flask import Flask
from flask import render_template

from . import tasks, utils, db


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
    )
    app.json_encoder = utils.JSONEncoder

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)
    from . import api
    app.register_blueprint(api.bp)

    @app.route('/')
    def render_index():
        return render_template('feed.html')

    return app

var posts = new Vue({
    el: "#posts",
    data: {
        posts_list: [],
    },
    delimiters: ['[[', ']]'],
    created() {
        this.loadData();
        setInterval(function () {
            console.log('loading data');
            this.loadData();
        }.bind(this), 30000);
    },
    methods: {
        loadData: function () {
            fetch('/api/get_recent_posts')
                .then((response) => {
                    if (response.ok) {
                        console.log('succsesful response');
                        return response.json()
                    }
                    console.log('not really successful');
                    throw new Error('Network response was not ok');
                })
                .then((json) => {
                    this.posts_list = [];
                    json.forEach(function (post) {
                        let video = [];
                        let photos = [];
                        post.attachments.forEach(function (attachment) {
                            let type = attachment.type;
                            if (type === "photo") {
                                photos.push(attachment.url)
                            } else {
                                video.push({url: attachment.url})
                            }
                        });
                        let date = new Date(post.date * 1000);
                        let day = date.getDate();
                        let month = date.getMonth() + 1;
                        let year = date.getFullYear();
                        let hours = date.getHours();
                        let minutes = "0" + date.getMinutes();

                        let formattedTime = day + '.' + month + '.' + year + ' ' + hours + ':' + minutes.substr(-2);

                        posts.posts_list.push({
                            text: post.text,
                            owner_id: post.owner_id,
                            video: video,
                            photos: photos,
                            name: post.name,
                            ava: post.ava,
                            date: formattedTime
                        })
                    })
                })
        }
    }
});
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for, jsonify
)

from flaskr.db import Database
from flaskr.follow_list import follow_list

bp = Blueprint('api', __name__, url_prefix='/api')


@bp.route('/get_recent_posts')
def get_recent_posts():
    db = Database().get_db()
    return jsonify(list(db.posts.find({'owner_id': {'$in': follow_list}}).sort('date', -1)))
